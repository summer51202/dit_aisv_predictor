#include "ImageSaveFailedException.h"


ImageSaveFailedException::ImageSaveFailedException()
{
	SetErrorMessage();
}
ImageSaveFailedException::ImageSaveFailedException(string error_message) : BaseCustomException(error_message)
{

}
void ImageSaveFailedException::SetErrorMessage()
{
	m_error_message = "image save failed";
}