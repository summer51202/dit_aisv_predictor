#include "AnomalyDetectionPredictor.h"


AnomalyDetectionPredictor::AnomalyDetectionPredictor(PredictorBuildInfo* p_build_info) : BasePredictor(p_build_info)
{

}
void AnomalyDetectionPredictor::SavePredictInfo()
{
	Logger(INFO) << "Saving the predict info of anomaly detection...";

	ofstream file;
	string result_txt_path = GetBuildInfo()->m_result_path + "/result_" + GetBuildInfo()->m_image_name + ".txt";

	file.open(result_txt_path);

	// Check file opened successful
	if (file.is_open())
		Logger(INFO) << "File " << result_txt_path << " opened successful.";
	else
	{
		Logger(ERROR) << "Error: File opened failed.";
		file.exceptions(ofstream::failbit | ofstream::badbit);
	}

	file << "================== Anomaly Detection Predict Info ==================" << endl;
	file << "Class Index:	" << GetPredictInfo()->anomalydetection_info.class_id << endl;
	file << "Class Name:	" << GetPredictInfo()->anomalydetection_info.name << endl;
	file << "Color(r, g, b):	(" << GetPredictInfo()->anomalydetection_info.color.r << ", " << GetPredictInfo()->anomalydetection_info.color.g << ", " << GetPredictInfo()->anomalydetection_info.color.b << ")" << endl;
	
	file.close();
}