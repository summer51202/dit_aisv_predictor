#pragma once

#include <iostream>
#include <ctime>
#include <string>
#include <vector>

using namespace std;


enum LogLevel
{
	DEBUG,
	INFO,
	WARNING,
	ERROR,
	SAVE
};

class Logger
{
	public:
		Logger(LogLevel);
		~Logger();
		template<class T>
		Logger& operator<<(const T&);
		static LogLevel& ShowVisibleLevel();
		static void SetVisibleLevel(LogLevel level);
		string GetNowTime();

	private:
		string GetStringLogLevel(LogLevel);

		bool m_message_is_visible = false;
		LogLevel m_message_level = DEBUG;
		time_t m_now_time;
};

#include "Logger.tpp"

