#include "SegmentationPredictor.h"


SegmentationPredictor::SegmentationPredictor(PredictorBuildInfo* p_build_info) : BasePredictor(p_build_info)
{

}
void SegmentationPredictor::SavePredictInfo()
{
	Logger(INFO) << "Saving the predict info of segmentation...";

	ofstream file;
	string result_txt_path = GetBuildInfo()->m_result_path + "/result_" + GetBuildInfo()->m_image_name + ".txt";

	file.open(result_txt_path);

	// Check file opened successful
	if (file.is_open())
		Logger(INFO) << "File " << result_txt_path << " opened successful.";
	else
	{
		Logger(ERROR) << "Error: File opened failed.";
		file.exceptions(ofstream::failbit | ofstream::badbit);
	}

	file << "==================== Segmentation Predict Info =====================" << endl;
	file << "Index	Name	Threshold	Color(r, g, b)" << endl;
	for (int i = 0; i < GetPredictInfo()->segmentation_info.category_size; i++)
	{
		file << GetPredictInfo()->segmentation_info.category_list[i].index << "	"
			<< GetPredictInfo()->segmentation_info.category_list[i].name << "	"
			<< GetPredictInfo()->segmentation_info.category_list[i].threshold << "		"
			<< "(" << GetPredictInfo()->segmentation_info.category_list[i].color.r
			<< ", " << GetPredictInfo()->segmentation_info.category_list[i].color.g
			<< ", " << GetPredictInfo()->segmentation_info.category_list[i].color.b << ")" << endl;
	}

	file.close();
}