#include "PredictorBuildInfo.h"


PredictorBuildInfo::PredictorBuildInfo(PredictorType type, string model_path, string image_path, string result_path, bool use_gpu, int gpu_index) : m_type(type), m_model_path(model_path), m_image_path(image_path), m_result_path(result_path), m_use_gpu(use_gpu), m_gpu_index(gpu_index)
{
	m_image_name = GetImageName();
}
string PredictorBuildInfo::GetImageName()
{
	int slash_pos = m_image_path.find_last_of("/");
	int dot_pos = m_image_path.find_last_of(".");
	string name = m_image_path.substr(slash_pos+1, dot_pos-slash_pos-1);
	return name;
}
string PredictorBuildInfo::ToString(PredictorType type)
{
	string str_type;
	switch (type)
	{
	case SEGMENTATION_PREDICTOR:
		str_type = "Segmentation Predictor";
		break;
	case OBJECT_DETECTION_PREDICTOR:
		str_type = "Object Detection Predictor";
		break;
	case CLASSIFICATION_PREDICTOR:
		str_type = "Classification Predictor";
		break;
	case ANOMALY_DETECTION_PREDICTOR:
		str_type = "Anomaly Detection Predictor";
		break;
	default:
		Logger(ERROR) << "The predictor type does not exist.";
		exit(1);
	}
	return str_type;
}
PredictorType PredictorBuildInfo::ToEnum(string str_type)
{
	if (str_type == "Segmentation Predictor") 
		return SEGMENTATION_PREDICTOR;
	else if (str_type == "Object Detection Predictor") 
		return OBJECT_DETECTION_PREDICTOR;
	else if (str_type == "Classification Predictor") 
		return CLASSIFICATION_PREDICTOR;
	else if (str_type == "Anomaly Detection Predictor") 
		return ANOMALY_DETECTION_PREDICTOR;
	else
	{
		Logger(ERROR) << "The predictor type does not exist.";
		exit(1);
	}
}