#pragma once

#include "BasePredictor.h"


class ObjectDetectionPredictor: public BasePredictor
{
	public:
		ObjectDetectionPredictor(PredictorBuildInfo*);

	private:
		void SavePredictInfo();
};

