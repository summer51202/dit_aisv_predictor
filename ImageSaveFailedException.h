#pragma once

#include <string>

#include "BaseCustomException.h"


class ImageSaveFailedException : public BaseCustomException
{
public:
	ImageSaveFailedException();
	ImageSaveFailedException(string);

protected:
	void SetErrorMessage();
};
