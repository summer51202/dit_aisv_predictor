#include "Logger.h"

#include <iostream>

template<class T>
Logger& Logger::operator<<(const T& message)
{
	if(m_message_level >= ShowVisibleLevel())
	{
		// show log message
		cout << message;
		m_message_is_visible = true;
	}
	return *this;
}
