#pragma once

#include "BasePredictor.h"


class SegmentationPredictor: public BasePredictor
{
	public:
		SegmentationPredictor(PredictorBuildInfo*);

	private:
		void SavePredictInfo();
};

