#include "ObjectDetectionPredictor.h"


ObjectDetectionPredictor::ObjectDetectionPredictor(PredictorBuildInfo* p_build_info) : BasePredictor(p_build_info)
{

}
void ObjectDetectionPredictor::SavePredictInfo()
{
	Logger(INFO) << "Saving the predict info of object detection...";

	ofstream file;
	string result_txt_path = GetBuildInfo()->m_result_path + "/result_" + GetBuildInfo()->m_image_name + ".txt";

	file.open(result_txt_path);

	// Check file opened successful
	if (file.is_open())
		Logger(INFO) << "File " << result_txt_path << " opened successful.";
	else
	{
		Logger(ERROR) << "Error: File opened failed.";
		file.exceptions(ofstream::failbit | ofstream::badbit);
	}

	file << "=============================== Object Detection Predict Info ================================" << endl;
	file << "Index	Name	BBox(topleft_x, topleft_y, width, height)	Probability	Color(r, g, b)" << endl;
	for (int i = 0; i < GetPredictInfo()->objectdetection_info.size; i++)
	{
		file << GetPredictInfo()->objectdetection_info.result_list[i].class_id << "	"
			<< GetPredictInfo()->objectdetection_info.result_list[i].name << "	"
			<< "(" << GetPredictInfo()->objectdetection_info.result_list[i].topleft_x
			<< ", " << GetPredictInfo()->objectdetection_info.result_list[i].topleft_y
			<< ", " << GetPredictInfo()->objectdetection_info.result_list[i].width
			<< ", " << GetPredictInfo()->objectdetection_info.result_list[i].height << ")		"
			<< GetPredictInfo()->objectdetection_info.result_list[i].prob << "	"
			<< "(" << GetPredictInfo()->objectdetection_info.result_list[i].c_color.r
			<< ", " << GetPredictInfo()->objectdetection_info.result_list[i].c_color.g
			<< ", " << GetPredictInfo()->objectdetection_info.result_list[i].c_color.b << ")" << endl;
	}

	file.close();
}