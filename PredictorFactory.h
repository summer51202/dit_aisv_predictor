#pragma once

#include <string>
#include <memory>

#include "BasePredictor.h"
#include "SegmentationPredictor.h"
#include "ObjectDetectionPredictor.h"
#include "ClassificationPredictor.h"
#include "AnomalyDetectionPredictor.h"

class PredictorFactory
{
	public:
		static unique_ptr<BasePredictor> CreatePredictor(PredictorBuildInfo*);
};

