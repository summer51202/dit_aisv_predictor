#include "BasePredictor.h"


BasePredictor::BasePredictor(PredictorBuildInfo* p_build_info): m_p_build_info(p_build_info), m_predictor(NULL), m_test_img(NULL), m_annotated_img(NULL), m_model_info(NULL), m_category_info(NULL), m_predict_info(NULL)
{
	Logger(INFO) << PredictorBuildInfo::ToString(m_p_build_info->m_type) << " is building...";
}
BasePredictor::~BasePredictor()
{
	if (m_predictor)
	{
		delete m_predictor;
		m_predictor = NULL;
	}

	FreeImage(m_test_img);
	m_test_img = NULL;

	FreeImage(m_annotated_img);
	m_annotated_img = NULL;

	FreePredictInfo(m_predict_info);
	m_predict_info = NULL;
}
void BasePredictor::LoadDitox()
{
	m_predictor = new Predictor();
	if (!m_predictor)
		Logger(ERROR) << "Object new failed.";
	m_predictor->LoadModel(m_p_build_info->m_model_path.c_str(), m_p_build_info->m_use_gpu, m_p_build_info->m_gpu_index);
	CheckModelIsLoaded();
}
void BasePredictor::LoadImage()
{
	CheckModelIsLoaded();
	m_test_img = (AisvImage*)malloc(sizeof(AisvImage));
	*m_test_img = m_predictor->ReadImage(m_p_build_info->m_image_path.c_str());
	if (!m_test_img)
	{
		Logger(ERROR) << "Image load failed.";
		throw ImageReadFailedException();
	}
}
void BasePredictor::Predict()
{
	CheckModelIsLoaded();
	// TODO: check test img is not null
	m_predict_info = (AisvPredictInfo*)malloc(sizeof(AisvPredictInfo));
	*m_predict_info = m_predictor->Predict(m_test_img);
	// TODO: check test img are not null 
	m_annotated_img = (AisvImage*)malloc(sizeof(AisvImage));
	*m_annotated_img = m_predictor->VisualizeResult(m_test_img, m_predict_info, false);
}
void BasePredictor::ShowDitoxInfo()
{
	CheckModelIsLoaded();
	m_model_info = (AisvModelInfo*)malloc(sizeof(AisvModelInfo));
	*m_model_info = m_predictor->GetModelInfo();
	m_category_info = (AisvCategoryInfo*)malloc(sizeof(AisvCategoryInfo));
	*m_category_info = m_predictor->GetCategoryInfo();
	cout << "============================ Model Info ============================" << endl;
	cout << "Model Type:	" << m_model_info->model_type << endl;
	cout << "Model Status:	" << m_model_info->model_status << endl;
	cout << "Project Name:	" << m_model_info->project_name << endl;
	cout << "Task Name:	" << m_model_info->task_name << endl;
	cout << endl;
	cout << "========================== Category Info ===========================" << endl;
	cout << "Name	Threshold	Color(r, g, b)" << endl;
	for (int i = 0; i < m_category_info->size; i++)
	{
		cout << m_category_info->category_list[i].name << "	"
			<< m_category_info->category_list[i].threshold << "		"
			<< "(" << m_category_info->category_list[i].color.r
			<< ", " << m_category_info->category_list[i].color.g
			<< ", " << m_category_info->category_list[i].color.b << ")" << endl;
	}
	cout << endl;
}
void BasePredictor::Save()
{
	CheckModelIsLoaded();
	// TODO: check annotated_img_ is not null
	if (_access(m_p_build_info->m_result_path.c_str(), 0) == -1) 
		_mkdir(m_p_build_info->m_result_path.c_str());
	SavePredictImg();
	SavePredictInfo();
}
void BasePredictor::SavePredictImg()
{
	string result_img_path = m_p_build_info->m_result_path + "/result_" + m_p_build_info->m_image_name + ".jpg";
	bool image_is_saved = false;
	image_is_saved = m_predictor->SaveImage(result_img_path.c_str(), m_annotated_img);
	if (!image_is_saved)
	{
		Logger(ERROR) << "Image save failed.";
		throw ImageSaveFailedException();
	}

}
void BasePredictor::CheckModelIsLoaded()
{
	if (!m_predictor->CheckSession())
	{
		Logger(ERROR) << "Load ditox model failed.";
		throw DitoxLoadFailedException();
	}
}
PredictorBuildInfo* BasePredictor::GetBuildInfo()
{
	return m_p_build_info;
}
AisvPredictInfo* BasePredictor::GetPredictInfo()
{
	return m_predict_info;
}
Predictor* BasePredictor::GetPredictor()
{
	return m_predictor;
}
AisvImage* BasePredictor::GetAnnotatedImage()
{
	return m_annotated_img;
}