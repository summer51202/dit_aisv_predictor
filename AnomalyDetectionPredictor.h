#pragma once

#include "BasePredictor.h"


class AnomalyDetectionPredictor : public BasePredictor
{
	public:
		AnomalyDetectionPredictor(PredictorBuildInfo*);

	private:
		void SavePredictInfo();
};