#pragma once

#include <string>
#include <direct.h>
#include <io.h>
#include <fstream>

#include "Logger.h"
#include "aisv_predictor.h"
#include "PredictorBuildInfo.h"
#include "DitoxLoadFailedException.h"
#include "ImageReadFailedException.h"
#include "ImageSaveFailedException.h"


using namespace std;
using namespace aisvision;


class BasePredictor
{
	public:
		virtual ~BasePredictor();
		void LoadDitox();
		void LoadImage();
		void Predict();
		void ShowDitoxInfo();
		void Save();
		PredictorBuildInfo* GetBuildInfo();
		AisvPredictInfo* GetPredictInfo();
		Predictor* GetPredictor();
		AisvImage* GetAnnotatedImage();

	protected:
		BasePredictor(PredictorBuildInfo*);

	private:
		void CheckModelIsLoaded();
		void SavePredictImg();
		virtual void SavePredictInfo() = 0;

		PredictorBuildInfo* m_p_build_info;
		Predictor* m_predictor;
		AisvImage* m_test_img;
		AisvImage* m_annotated_img;
		AisvModelInfo* m_model_info;
		AisvCategoryInfo* m_category_info;
		AisvPredictInfo* m_predict_info;
};

