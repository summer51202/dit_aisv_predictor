#pragma once

#include <string>

#include "BaseCustomException.h"


class ImageReadFailedException : public BaseCustomException
{
public:
	ImageReadFailedException();
	ImageReadFailedException(string);

protected:
	void SetErrorMessage();
};
