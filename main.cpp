#include <iostream>

#include "Logger.h"
#include "BasePredictor.h"
#include "PredictorFactory.h"

using namespace std;


void RunPredictor(const int kTaskNumber, bool use_gpu, int gpu_index, PredictorType predictor_type_arr[], string model_path_arr[], string image_path_arr[], string result_path)
{
	for (int i = 0; i < kTaskNumber; i++)
	{
		PredictorBuildInfo* p_build_info = new PredictorBuildInfo(predictor_type_arr[i], model_path_arr[i], image_path_arr[i], result_path, use_gpu, gpu_index);
		if (!p_build_info)
			Logger(ERROR) << "Object new failed.";

		// Construct specified predictor by build_info
		unique_ptr<BasePredictor> p_predictor = PredictorFactory::CreatePredictor(p_build_info);

		// Load
		p_predictor->LoadDitox();
		p_predictor->LoadImage();

		// Predict the test image with the ditox model
		p_predictor->Predict();

		// Print model info and category info of the ditox model
		p_predictor->ShowDitoxInfo();

		// Save the predict result of the test image
		p_predictor->Save();

		if (p_build_info)
		{
			delete p_build_info;
			p_build_info = NULL;
		}
	}
}

// Example commands DITAisvPredictor.exe

int main(int argc, char* argv[])
{
	Logger::SetVisibleLevel(DEBUG);

	const int kTaskNumber = 4;
	bool use_gpu = true;
	int gpu_index = 0;
	PredictorType predictor_type_arr[kTaskNumber];
	string model_path_arr[kTaskNumber];
	string image_path_arr[kTaskNumber];
	string result_path = "D:/Edward/projects/CPlusPlus/DITAisvPredictor/DITAisvPredictor/result";

	predictor_type_arr[0] = SEGMENTATION_PREDICTOR;
	predictor_type_arr[1] = OBJECT_DETECTION_PREDICTOR;
	predictor_type_arr[2] = CLASSIFICATION_PREDICTOR;
	predictor_type_arr[3] = ANOMALY_DETECTION_PREDICTOR;

	model_path_arr[0] = "D:/Edward/projects/CPlusPlus/DITAisvPredictor/DITAisvPredictor/example_data/Segmentation/model/Segmentation.ditox";
	model_path_arr[1] = "D:/Edward/projects/CPlusPlus/DITAisvPredictor/DITAisvPredictor/example_data/ObjectDetection/model/ObjectDetection.ditox";
	model_path_arr[2] = "D:/Edward/projects/CPlusPlus/DITAisvPredictor/DITAisvPredictor/example_data/Classification/model/Classification.ditox";
	model_path_arr[3] = "D:/Edward/projects/CPlusPlus/DITAisvPredictor/DITAisvPredictor/example_data/AnomalyDetection/model/AnomalyDetection.ditox";

	image_path_arr[0] = "D:/Edward/projects/CPlusPlus/DITAisvPredictor/DITAisvPredictor/example_data/Segmentation/image/Class2_0003.png";
	image_path_arr[1] = "D:/Edward/projects/CPlusPlus/DITAisvPredictor/DITAisvPredictor/example_data/ObjectDetection/image/0022.png";
	image_path_arr[2] = "D:/Edward/projects/CPlusPlus/DITAisvPredictor/DITAisvPredictor/example_data/Classification/image/NG/1158.png";
	image_path_arr[3] = "D:/Edward/projects/CPlusPlus/DITAisvPredictor/DITAisvPredictor/example_data/AnomalyDetection/image/NG/Class9_0034.png";
	
	try
	{
		RunPredictor(kTaskNumber, use_gpu, gpu_index, predictor_type_arr, model_path_arr, image_path_arr, result_path);
	}
	catch (const bad_alloc& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	catch (const ios_base::failure& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	catch (const DitoxLoadFailedException& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	catch (const ImageReadFailedException& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	catch (const ImageSaveFailedException& e)
	{
		Logger(ERROR) << "Exception occured: " << e.what();
	}
	// Only in release version
	//catch (...)
	//{
	//	Logger(ERROR) << "Unknown exception occured.";
	//}

	system("pause");
	return 0;
}