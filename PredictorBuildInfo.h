#pragma once

#include <string>

#include "Logger.h"

using namespace std;


enum PredictorType
{
	SEGMENTATION_PREDICTOR,
	OBJECT_DETECTION_PREDICTOR,
	CLASSIFICATION_PREDICTOR,
	ANOMALY_DETECTION_PREDICTOR
};

class PredictorBuildInfo
{
	public:
		PredictorBuildInfo(PredictorType, string, string, string, bool, int);
		static string ToString(PredictorType);
		static PredictorType ToEnum(string);

	private:
		string GetImageName();

	public:
		PredictorType m_type;
		string m_model_path;
		string m_image_path;
		string m_image_name;
		string m_result_path;
		bool m_use_gpu;
		int m_gpu_index;
};

