#pragma once

#include <string>

#include "BaseCustomException.h"


class DitoxLoadFailedException : public BaseCustomException
{
	public:
		DitoxLoadFailedException();
		DitoxLoadFailedException(string);

	protected:
		void SetErrorMessage();
};

