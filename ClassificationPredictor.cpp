#include "ClassificationPredictor.h"


ClassificationPredictor::ClassificationPredictor(PredictorBuildInfo* p_build_info) : BasePredictor(p_build_info)
{

}
void ClassificationPredictor::SavePredictInfo()
{
	Logger(INFO) << "Saving the predict info of classification...";

	ofstream file;
	string result_txt_path = GetBuildInfo()->m_result_path + "/result_" + GetBuildInfo()->m_image_name + ".txt";

	file.open(result_txt_path);

	// Check file opened successful
	if (file.is_open())
		Logger(INFO) << "File " << result_txt_path << " opened successful.";
	else
	{
		Logger(ERROR) << "Error: File opened failed.";
		file.exceptions(ofstream::failbit | ofstream::badbit);
	}

	file << "==================== Classification Predict Info ===================" << endl;
	file << "Class Index:	" << GetPredictInfo()->classification_info.class_id << endl;
	file << "Class Name:	" << GetPredictInfo()->classification_info.name << endl;
	file << "Probability:	" << GetPredictInfo()->classification_info.prob << endl;
	file << "Color(r, g, b):	(" << GetPredictInfo()->classification_info.color.r << ", " << GetPredictInfo()->classification_info.color.g << ", " << GetPredictInfo()->classification_info.color.b << ")" << endl;

	file.close();
}