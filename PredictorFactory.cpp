#include "PredictorFactory.h"


unique_ptr<BasePredictor> PredictorFactory::CreatePredictor(PredictorBuildInfo* p_build_info)
{	
	unique_ptr<BasePredictor> p_predictor;

	switch (p_build_info->m_type)
	{
	case SEGMENTATION_PREDICTOR:
		p_predictor = make_unique<SegmentationPredictor>(p_build_info);
		break;
	case OBJECT_DETECTION_PREDICTOR:
		p_predictor = make_unique<ObjectDetectionPredictor>(p_build_info);
		break;
	case CLASSIFICATION_PREDICTOR:
		p_predictor = make_unique<ClassificationPredictor>(p_build_info);
		break;
	case ANOMALY_DETECTION_PREDICTOR:
		p_predictor = make_unique<AnomalyDetectionPredictor>(p_build_info);
		break;
	}

	if (!p_predictor)
		Logger(ERROR) << "Object new failed.";

	return p_predictor;
}