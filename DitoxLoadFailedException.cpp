#include "DitoxLoadFailedException.h"


DitoxLoadFailedException::DitoxLoadFailedException()
{
	SetErrorMessage();
}
DitoxLoadFailedException::DitoxLoadFailedException(string error_message) : BaseCustomException(error_message)
{

}
void DitoxLoadFailedException::SetErrorMessage()
{
	m_error_message = "ditox model read failed";
}