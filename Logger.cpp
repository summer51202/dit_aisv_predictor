#include "Logger.h"

#include <iostream>


Logger::Logger(LogLevel level) : m_message_level(level)
{
	operator << ("[" + GetStringLogLevel(m_message_level) + "]") << ("[" + GetNowTime() + "]");
}
Logger::~Logger()
{
	if (m_message_is_visible) 
		cout << endl;
}
string Logger::GetStringLogLevel(LogLevel level)
{
	string string_level;
	switch (level)
	{
	case DEBUG:
		string_level = "DEBUG";
		break;
	case INFO:
		string_level = "INFO";
		break;
	case WARNING:
		string_level = "WARNING";
		break;
	case ERROR:
		string_level = "ERROR";
		break;
	case SAVE:
		string_level = "SAVE";
		break;
	}
	return string_level;
}
LogLevel& Logger::ShowVisibleLevel()
{
	static LogLevel level = INFO;
	return level;
}
void Logger::SetVisibleLevel(LogLevel level)
{
	ShowVisibleLevel() = level;
}
string Logger::GetNowTime()
{
	/* struct tm data structure
	struct tm {
		int tm_sec;		// second: [0,59]
		int tm_min;		// minute: [0,59]
		int tm_hour;	// hour: [0,23]
		int tm_mday;	// day in month: [1,31]
		int tm_mon;		// month: [0,11]
		int tm_year;	// year: current year - 1990
		int tm_wday;	// week: [0,6]
		int tm_yday;	// day in year: [0,365]
		int tm_isdst;	// daylight saving time flag. Positive: is DST; Zero: is not DST; Negative: check if is DST by mktime()
	};*/
	time(&m_now_time);

	struct tm time_info;
	localtime_s(&time_info, &m_now_time);

	string s, m, h, D, M, Y;
	s = to_string(time_info.tm_sec);
	m = to_string(time_info.tm_min);
	h = to_string(time_info.tm_hour);
	D = to_string(time_info.tm_mday);
	M = to_string(time_info.tm_mon + 1);
	Y = to_string(time_info.tm_year + 1900);

	// Aligned output format  
	if (time_info.tm_sec < 10) s = "0" + s;
	if (time_info.tm_min < 10) m = "0" + m;
	if (time_info.tm_hour < 10) h = "0" + h;
	if (time_info.tm_mday < 10) D = "0" + D;
	if (time_info.tm_mon + 1 < 10) M = "0" + M;

	return (Y + M + D + "-" + h + "h" + m + "m" + s + "s");
}