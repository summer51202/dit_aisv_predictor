#pragma once

#include "BasePredictor.h"


class ClassificationPredictor : public BasePredictor
{
	public:
		ClassificationPredictor(PredictorBuildInfo*);

	private:
		void SavePredictInfo();
};